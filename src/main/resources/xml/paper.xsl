<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <h2>Papers</h2>
            <table border="1">
                <tr>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Mounthly</th>
                    <th>Colour</th>
                    <th>Amount of pages</th>
                    <th>Is glance</th>
                    <th>Have index</th>
                </tr>


                <xsl:for-each select="papers/paper">
                    <xsl:sort select="chars/amountOfPages"
                    data-type="number"/>
                    <tr>
                        <td>
                            <xsl:value-of select="title"/>
                        </td>
                        <td>
                            <xsl:value-of select="type"/>
                        </td>
                        <td>
                            <xsl:value-of select="mouthly"/>
                        </td>
                        <td>
                            <xsl:value-of select="chars/color"/>
                        </td>
                        <td>
                            <xsl:value-of select="chars/amountOfPages"/>
                        </td>
                        <td>
                            <xsl:value-of select="chars/glance"/>
                        </td>
                        <td>
                            <xsl:value-of select="chars/haveIndex"/>
                        </td>
                    </tr>
                </xsl:for-each>

            </table>

        </html>


    </xsl:template>
</xsl:stylesheet>