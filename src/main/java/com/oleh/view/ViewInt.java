package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void resForDOM();
    void resForSAX();
    void resForStAX();
}
