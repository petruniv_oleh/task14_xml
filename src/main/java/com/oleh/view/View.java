package com.oleh.view;

import com.oleh.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt{

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. DOM parser");
        menuMap.put("2", "2. SAX parser");
        menuMap.put("3", "3. StAX parser");


        menuMapMethods.put("1", this::resForDOM);
        menuMapMethods.put("2", this::resForSAX);
        menuMapMethods.put("3", this::resForStAX);


    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();

            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }


    @Override
    public void resForDOM() {
        logger.info("show results of DOM parser");
        try {
            printList(controller.getDomParsedList());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resForSAX() {
        logger.info("show results of DOM parser");
        try {
            printList(controller.getSAXParsedList());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resForStAX() {
        logger.info("show results of DOM parser");
        try {
            printList(controller.getStAXParsedList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }

    private void printList(List list){
        for (Object o:list
             ) {
            System.out.println(o);
        }
    }
}
