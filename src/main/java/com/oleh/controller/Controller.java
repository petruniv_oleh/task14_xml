package com.oleh.controller;

import com.oleh.model.Paper;
import com.oleh.model.parser.DOM;
import com.oleh.model.parser.SAX;
import com.oleh.model.parser.StAX;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Controller implements ControllerInt {

    @Override
    public List<Paper> getDomParsedList() throws IOException, SAXException, ParserConfigurationException {
        return new DOM().getPapers();
    }

    @Override
    public List<Paper> getSAXParsedList() throws IOException, SAXException, ParserConfigurationException {
        return new SAX().getPapers();
    }

    @Override
    public List<Paper> getStAXParsedList() throws FileNotFoundException, XMLStreamException {
        return new StAX().getPapers();
    }
}
