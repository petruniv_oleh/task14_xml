package com.oleh.controller;

import com.oleh.model.Paper;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface ControllerInt {
    List<Paper> getDomParsedList() throws IOException, SAXException, ParserConfigurationException;
    List<Paper> getSAXParsedList() throws IOException, SAXException, ParserConfigurationException;
    List<Paper> getStAXParsedList() throws FileNotFoundException, XMLStreamException;
}
