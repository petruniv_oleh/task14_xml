package com.oleh.model.parser;

import com.oleh.model.Chars;
import com.oleh.model.Paper;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StAX {

    XMLInputFactory xmlInputFactory;
    XMLEventReader xmlEventReader;
    List<Paper> papers;

    public StAX() throws FileNotFoundException, XMLStreamException {
        papers = new ArrayList<Paper>();
        xmlInputFactory = XMLInputFactory.newInstance();
        xmlEventReader = xmlInputFactory.createXMLEventReader(
                new FileReader(new File("src/main/resources/xml/paper.xml")));
        parse();

    }

    private void parse() throws XMLStreamException {

        Paper paper = null;
        Chars chars = null;
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();

                if (startElement.getName().getLocalPart().equals("paper")) {
                    paper = new Paper();
                    chars = new Chars();
                }

                Iterator<Attribute> attributes = startElement.getAttributes();

                if (startElement.getName().getLocalPart().equals("title")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    paper.setTitle(data.getData());
                } else if (startElement.getName().getLocalPart().equals("type")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    paper.setType(data.getData());
                }
                if (startElement.getName().getLocalPart().equals("mouthly")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    paper.setMouthly(Boolean.parseBoolean(data.getData()));
                }
                if (startElement.getName().getLocalPart().equals("color")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    chars.setColor(Boolean.parseBoolean(data.getData()));
                }
                if (startElement.getName().getLocalPart().equals("amountOfPages")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    chars.setAmountOfPages(Integer.parseInt(data.getData()));
                }
                if (startElement.getName().getLocalPart().equals("glance")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    chars.setGlance(Boolean.parseBoolean(data.getData()));
                }
                if (startElement.getName().getLocalPart().equals("haveIndex")) {
                    Characters data = (Characters) xmlEventReader.nextEvent();
                    chars.setHaveIndex(Boolean.parseBoolean(data.getData()));
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("paper")) {
                    paper.setChars(chars);
                    papers.add(paper);
                    paper = null;
                    chars = null;
                }
            }
        }

    }

    public List<Paper> getPapers() {
        papers.sort(new PapersComparator());
        return papers;
    }
}
