package com.oleh.model.parser;

import com.oleh.model.Chars;
import com.oleh.model.Paper;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAX {

    SAXParserFactory factory;
    SAXParser parser;
    List<Paper> papers;


    public SAX() throws ParserConfigurationException, SAXException, IOException {
        factory = SAXParserFactory.newInstance();
        parser = factory.newSAXParser();
        papers = new ArrayList<Paper>();
        XMLHandler xmlHandler = new XMLHandler();
        parser.parse(new File("src/main/resources/xml/paper.xml"), xmlHandler);
    }

    public List<Paper> getPapers() {
        papers.sort(new PapersComparator());
        return papers;
    }

    class XMLHandler extends DefaultHandler {
        private String elementName;
        private String title;
        private String type;
        private Boolean mounthly;
        private Boolean color;
        private Integer amountOfPages;
        private Boolean glance;
        private Boolean haveIndex;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            elementName = qName;
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (title != null && type != null && mounthly != null && color != null && amountOfPages != null
                    && glance != null && haveIndex != null) {
                Chars chars = new Chars(color, amountOfPages, glance, haveIndex);
                papers.add(new Paper(title, type, mounthly, chars));
                title = null;
                type = null;
                mounthly = null;
                color = null;
                amountOfPages = null;
                glance = null;
                haveIndex = null;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String data = new String(ch, start, length);
            data = data.replace("\n", "").trim();

            if (!data.isEmpty()) {
                if (elementName.equals("title")) {
                    title = data;
                } else if (elementName.equals("type")) {
                    type = data;
                } else if (elementName.equals("mouthly")) {
                    mounthly = Boolean.parseBoolean(data);
                } else if (elementName.equals("color")) {
                    color = Boolean.parseBoolean(data);
                } else if (elementName.equals("amountOfPages")) {
                    amountOfPages = Integer.parseInt(data);
                } else if (elementName.equals("glance")) {
                    glance = Boolean.parseBoolean(data);
                } else if (elementName.equals("haveIndex")) {
                    haveIndex = Boolean.parseBoolean(data);
                }
            }
        }
    }

}
