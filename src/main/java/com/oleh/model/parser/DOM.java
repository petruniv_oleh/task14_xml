package com.oleh.model.parser;

import com.oleh.model.Chars;
import com.oleh.model.Paper;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOM {
    private List<Paper> papers;
    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder builder;
    Document document;
    public DOM() throws ParserConfigurationException, IOException, SAXException {
        papers = new ArrayList<Paper>();
        builderFactory = DocumentBuilderFactory.newInstance();
        builder = builderFactory.newDocumentBuilder();
        document = builder.parse(new File("src/main/resources/xml/paper.xml"));
        parse();

    }

    private void parse() throws IOException, SAXException {

        NodeList paperList = document.getDocumentElement().getElementsByTagName("paper");

        for (int i = 0; i < paperList.getLength(); i++) {
            Element item = (Element) paperList.item(i);
            Element chars = (Element) item.getElementsByTagName("chars").item(0);

            papers.add(new Paper(
                    item.getElementsByTagName("title").item(0).getTextContent(),
                    item.getElementsByTagName("type").item(0).getTextContent(),
                    Boolean.parseBoolean( item.getElementsByTagName("mouthly").item(0).getTextContent()),
                    new Chars(
                            Boolean.parseBoolean(chars.getElementsByTagName("color").item(0).getTextContent()),
                            Integer.parseInt(chars.getElementsByTagName("amountOfPages").item(0).getTextContent()),
                            Boolean.parseBoolean(chars.getElementsByTagName("glance").item(0).getTextContent()),
                            Boolean.parseBoolean(chars.getElementsByTagName("haveIndex").item(0).getTextContent())
                    )
            ));



        }
    }




    public List<Paper> getPapers() {
        papers.sort(new PapersComparator());
        return papers;
    }
}
