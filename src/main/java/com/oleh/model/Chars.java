package com.oleh.model;

public class Chars {
    private boolean color;
    private int amountOfPages;
    private boolean glance;
    private boolean haveIndex;

    public Chars() {
    }

    public Chars(boolean color, int amountOfPages, boolean glance, boolean haveIndex) {
        this.color = color;
        this.amountOfPages = amountOfPages;
        this.glance = glance;
        this.haveIndex = haveIndex;
    }

    public boolean isColor() {
        return color;
    }

    public void setColor(boolean color) {
        this.color = color;
    }

    public int getAmountOfPages() {
        return amountOfPages;
    }

    public void setAmountOfPages(int amountOfPages) {
        this.amountOfPages = amountOfPages;
    }

    public boolean isGlance() {
        return glance;
    }

    public void setGlance(boolean glance) {
        this.glance = glance;
    }

    public boolean isHaveIndex() {
        return haveIndex;
    }

    public void setHaveIndex(boolean haveIndex) {
        this.haveIndex = haveIndex;
    }

    @Override
    public String toString() {
        return "Chars{" +
                "color=" + color +
                ", amountOfPages=" + amountOfPages +
                ", glance=" + glance +
                ", haveIndex=" + haveIndex +
                '}';
    }
}
